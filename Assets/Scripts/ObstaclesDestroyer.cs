﻿using UnityEngine;
using System.Collections;

public class ObstaclesDestroyer : MonoBehaviour {
    PrefabsPool prefabsPool;
    public void Start()
    {
        prefabsPool = PrefabsPool.Instance;
    }
    void OnTriggerEnter(Collider col)
    {
        prefabsPool.PutToPool(col.gameObject);
    }

    void OnCollisionStay(Collision col)
    {
        prefabsPool.PutToPool(col.gameObject);
    }
}
