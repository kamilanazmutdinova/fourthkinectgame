﻿using UnityEngine;
using System.Collections;

public class FailureController : MonoBehaviour {
    public float timer = 1f;
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        if (timer < 0f)
            this.gameObject.SetActive(false);
	}

    public void Failure(float t)
    {
        timer = t;
        this.gameObject.SetActive(true);
    }
}
