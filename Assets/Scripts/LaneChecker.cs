﻿using UnityEngine;
using System.Collections;

public class LaneChecker : MonoBehaviour {
    public int id;
    public PersonageController pc;
    void OnTriggerStay(Collider coll)
    {
        pc.checkedLanes[id] = false;
    }
    void OnTriggerExit(Collider coll)
    {
        pc.checkedLanes[id] = true;
    }

    
}
